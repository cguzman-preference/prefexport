/**
 * @file PrefExportUSD.h
 *
 * @brief This file contains the necessary code to declare the classs to export to USD
 *
 * @defgroup PrefExportBase
 * (Note: this needs exactly one @defgroup somewhere)
 *
 * @author Cesar Augusto Guzman ALvarez
 * Contact: cguzman@preference.es
 *
 */
#ifndef PREF_EXPORT_EXPORT_USD_
#define PREF_EXPORT_EXPORT_USD_

#include "PrefExportBase.h"


// IMPORT THIRD-PARTY LIBRARIES
#include <pxr/usd/usd/stage.h>
#include <pxr/usd/usdGeom/sphere.h>

#include <pxr/base/vt/value.h>
#include <pxr/base/vt/array.h>
#include <pxr/base/vt/dictionary.h>
#include <pxr/usd/sdf/assetPath.h>
#include <pxr/usd/sdf/path.h>
#include <pxr/usd/usd/modelAPI.h>

// for the example 3
#ifdef WIN32
#define OS_SEP '\\'
#else
#define OS_SEP '/'
#endif

#ifndef ASSET_DIRECTORY
	#define ASSET_DIRECTORY "."
#endif



// IMPORT THIRD-PARTY LIBRARIES
#include <pxr/base/tf/token.h>
#include <pxr/usd/kind/registry.h>
#include <pxr/usd/sdf/types.h>
#include <pxr/usd/usdGeom/mesh.h>
#include <pxr/usd/usdGeom/metrics.h>
#include <pxr/usd/usdGeom/tokens.h>
#include <pxr/usd/usdGeom/xform.h>
#include <pxr/usd/usdShade/material.h>
#include <pxr/usd/usdShade/materialBindingAPI.h>


/**
* This class implements the main functionality to export to USD. 
* 
* It employs the USD library
* 
* @author Cesar Guzman Alvarez
* mail: cguzman@preference.es
* created date: 21/12/2020
* last modified: 7/01/2021
* 
* version: 0.0.2
*/
class PrefExportUSD : public PrefExportBase {


public:
	// ------------------
	// Public section
	// ------------------

	// ###### CONSTRUCTOR & DESTRUCTOR #######

		// Initialises the parameters file_path and params
		PrefExportUSD();

		/**
		* @param file_path : file to store the usd
		*/
		PrefExportUSD(CPrefModelSink modelSink, const char* file_path);



	// ###### MAIN FUNCTION TO EXECUTE THE EXPORT #######

		/** start point of to export and preference's scene to USD
		* @return true is everything goes well
		*/
		bool exportDocument();

		/** create and add an scene
		* 
		* We create the scene as a simple model
		* 
		* 1. making the model
		* 
		* 2. adding a mesh
		* 
		* 3. make the material
		*
		* 4. add UsdPreviewSurface
		* 
		* 5. add texturing
		* 
		* @param preference's scene
		* @return true if everything goes well
		*/
		bool addScene(CPrefModelSink::Scene scene);




	// ##### Example codes : https://github.com/ColinKennedy/USD-Cookbook #####
		
		void example1();
		void example2();

		void example3MeshWithMaterials();
		pxr::UsdGeomMesh attach_billboard(pxr::UsdStageRefPtr& stage,
			std::string const& root,
			std::string const& name = "card");

private:
	// ------------------
	// Private section
	// ------------------

	// ###### PARAMETERS #######

		// store all the usd format
		pxr::UsdStageRefPtr usd_stage;


		// [ACCESOR-BUFFER] 
		// To optimize size in the gltf, this map will store the accessor indices (so geometry will be inserted only once)
		// It may happen that a mesh is too big for gltf and needs to be splitted into smaller meshes, hence that the keys may be multiple (storing multiple indices instead of one)
		struct accessorIndexS {
			std::vector<int> indicesAccIndex;
			std::vector<int> posAccIndex;
			std::vector<int> normalAccIndex;

			std::unordered_map<float, std::vector<int>> UVFactorDictAccIndex;
		};
		std::unordered_map<int, accessorIndexS> accessorsMeshInstanceDict;

		
		// map to control the created materials
		using Map = std::map<std::string, pxr::UsdShadeMaterial>;
		Map materials_;


	// ###### FUNCTIONS #######

		/** create and add a mesh
		* 
		* In a mesh we add the positions, normal, and UVs
		* 
		* @param pxr::UsdGeomMesh created before and in this function we add some attributes
		* @param CPrefModelSink::Mesh preference's scene
		* @param accessorIndex - no longer neccessary
		* @param factor for uv's parameter
		* @param mesh piece number - no longer neccessary
		* @param index of the mesh in the preference's scene
		*/
		int createMesh(pxr::UsdGeomMesh& mesh_USD, const CPrefModelSink::Mesh& mesh, 
			// accessorIndexS& accIndex, 
			const float& uvFactor, 
			// const int& meshPieceNumber, 
			const int& origMeshIndex);

		pxr::GfRange3f BoundPoints(const pxr::GfVec3f* points, size_t point_count);

		/** Create the materials in the mesh. 
		*
		* @param information about the mesh in usd
		* @param CPrefModelSink::Material of preference's scene
		*/
		void createMaterial(pxr::UsdGeomMesh meshUSD, CPrefModelSink::Material material);

		/** Fill in materials in the mesh
		* 
		* Only implement for metallic and roughness.!
		* 
		* @param surface shader, which is a container for the shading network.
		* @param CPrefModelSink::Material of preference's scene
		*/
		pxr::UsdShadeShader fillInMaterial(pxr::UsdShadeMaterial &materialUSD, CPrefModelSink::Material material);

		/** - Example code. TODO 
		* 
		* add a texture as the source for the surface's diffuseColor. 
		* Texturing requires two nodes in the shading network: a UsdUVTexture node 
		* to read and map the texture, and a UsdPrimvarReader 
		* (more precisely, a UsdPrimvarReader_float2, the implementation that reads 
		* float2Array-valued primvars) to fetch a texture coordinate from 
		* each piece of geometry bound to the material, to inform the texture node 
		* how to map surface coordinates to texture coordinates.
		*/
		pxr::UsdShadeShader attach_texture(pxr::UsdStageRefPtr& stage,
			pxr::UsdShadeShader shader,
			std::string const& material_path,
			std::string const reader_name = "stReader",
			std::string const shader_name = "diffuseTexture");

		/** add values in the UsdAttributes 
		* 
		* it considers to emulate the double sided
		* 
		* @param attribute of the mesh
		* @param array values to add to the attribute
		* @param true if it needs to emulate double sided
		*/
		template <typename Value, typename Attr>
		void SetVertexValues(const Attr& attr, const pxr::VtArray<Value> values, bool emulate_double_sided) {

			pxr::VtArray<Value> valOrig(values);

			// if emulate double sided
			if (emulate_double_sided) {
				// get the size of the positions in count
				const size_t count = values.size();

				// create an array with twice the value of count
				pxr::VtArray<Value> doubledValues(2*count);

				// save the positions from i = 0 to count - 1 and from count to count * 2
				for (size_t index = 0; index != count; index++) {
					doubledValues[index] = values[index];

					doubledValues[count + index] = values[index];
				}

				valOrig = doubledValues;

			}

			// save the positions
			attr.Set(valOrig);

		}

protected:
	// ------------------
	// Protected section
	// ------------------


};

#endif // !PREF_EXPORT_EXPORT_USD_