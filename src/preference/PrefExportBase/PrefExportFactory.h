/**
 * @file PrefExportFactory.h
 *
 * @brief This file contains the common code
 *
 * @ingroup PrefExportFactory
 * (Note: this needs exactly one @defgroup somewhere)
 *
 * @author Cesar Augusto Guzman ALvarez
 * Contact: cguzman@preference.es
 *
 */

 /**
 * This class implements a factory to create concrete classes
 *
 * author: Cesar Guzman Alvarez
 * mail: cguzman@preference.es
 * created date: 08/01/2021
 * last modified: 08/01/2021
 * version: 0.0.1
 **/
#ifndef PREF_EXPORT_EXPORT_FACTORY_
#define PREF_EXPORT_EXPORT_FACTORY_

// #include "PrefExportBase.h"
#include "PrefExportUSD.h"

using namespace PrefDirectX;

class PrefExportFactory
{

	public: 

	// ------------------
	// Public section
	// ------------------

	virtual PrefExportBase* Create(ExportFileFormats format);


};

#endif // !PREF_EXPORT_EXPORT_FACTORY_