/**
 * @file PrefExportUSD.cpp
 *
 * @brief This file contains the necessary code to export to USD
 *
 * @ingroup PrefExportBase
 * (Note: this needs exactly one @defgroup somewhere)
 *
 * @author Cesar Augusto Guzman ALvarez
 * Contact: cguzman@preference.es
 *
 */
 // for information see its associated .h
#include "PrefExportUSD.h"

using namespace std;

PrefExportUSD::PrefExportUSD() : PrefExportBase()
{
    example3MeshWithMaterials();
}

PrefExportUSD::PrefExportUSD(CPrefModelSink modelSink, const char* file_path) : PrefExportBase(modelSink, file_path)
{
}



bool PrefExportUSD::exportDocument() 
{
    bool create_file = false;
    
    printf("Name of the file %s ", getFilePath());

    // create the file usd
    usd_stage = pxr::UsdStage::CreateNew(getFilePath());
    
    if (!usd_stage) {
        /* This happens when the USD JSON files cannot be found. When that happens,
         * the USD library doesn't know it has the functionality to write USDA and
         * USDC files, and creating a new UsdStage fails. */
        throw invalid_argument("USD Export: unable to find suitable USD plugin to write.");
    }

    // set metadata #usda 1.0 ( .... ) 
    usd_stage->SetMetadata(pxr::UsdGeomTokens->upAxis, pxr::VtValue(pxr::UsdGeomTokens->z));
    usd_stage->SetMetadata(pxr::UsdGeomTokens->metersPerUnit, pxr::VtValue(10));

    // fill the keyword doc
    usd_stage->GetRootLayer()->SetDocumentation(std::string("Preference v") + getPExportVersion());

    
    // fill all the scenes - TODO
    addScenes(-1);


    // store in the file usd
    usd_stage->GetRootLayer()->Save();
    create_file = true;

    return create_file;

}

bool PrefExportUSD::addScene(CPrefModelSink::Scene scene) {
   
    /** 
        *1. making the model
        *
        *2. adding mesh
        *
        *3. make the material
        *
        *4. add UsdPreviewSurface
        *
        *5. add texturing
        * */

    // maximum number of indexes
    uint32_t maxIndex = 65535;

    // fx::gltf::Scene gltfScene;
    // 1. making a model
    auto root = pxr::UsdGeomXform::Define(usd_stage, pxr::SdfPath("/TexModel"));
    pxr::UsdModelAPI(root).SetKind(pxr::KindTokens->component);


    // TODO: Nodes and scenes can be much more complex -and include transformations-, 
    // this can be improved
    for (int primitiveIndex : scene.primitiveListIndices) {
        
        // get the primitive by index
        CPrefModelSink::Primitive primitive;
        getModelSink().getPrimitiveByIndex(primitiveIndex, primitive);
        

        // 2. Adding meshes

        // for each mesh 
        for (int meshInstanceIndex : primitive.meshInstanceIndices) {

            // get the mesh instance by index
            CPrefModelSink::MeshInstance meshInstance;
            getModelSink().getMeshInstanceByIndex(meshInstanceIndex, meshInstance);

            // get the mesh
            CPrefModelSink::Mesh mesh;
            getModelSink().getMeshByIndex(meshInstance.meshIndex, mesh);

            // create a mesh
            // fx::gltf::Node gltfNode;
            // fx::gltf::Mesh gltfMesh;
            // gltfNode.mesh = (uint32_t)document.meshes.size();
            const char* name = "card" + meshInstanceIndex;
            auto mesh_USD = pxr::UsdGeomMesh::Define(usd_stage, pxr::SdfPath(root.GetPath().GetString() + "/" + name));
            

            //Transformation
            if (meshInstance.transformationMatrix.size1() == 4 && meshInstance.transformationMatrix.size2() == 4) {
                for (int i = 0; i < meshInstance.transformationMatrix.size1(); i++) {
                    for (int j = 0; j < meshInstance.transformationMatrix.size2(); j++) {
                       // gltfNode.matrix[(i * 4) + j] = meshInstance.transformationMatrix(i, j); // TODO -with my variable mesh
                    }
                }

            }


            // Add to scene the node index value
            // [NO_NECESSARY] This step is not necessary in USD 
            // gltfScene.nodes.push_back((uint32_t)document.nodes.size());

            // Meshes & materials (first split the mesh -if required-, then add it)
            std::vector<CPrefModelSink::Mesh> meshList;


            // NOTE ABOUT SPLIT CODE : Currently, the following code is a working code. However, since in principle it is a GLTF limitation, I avoid this code. 
            //The number of vertex indices is over 65535, we need to split the mesh into smaller pieces
            //This is because gltf does not accept indices that are over the size of a short int (2 bytes)
            //Meshes in prefsuite always have the indices incrementing by 1, so anything over 65535 elements will have indices over that value
            /*if (mesh.vertexIndices.size() > maxIndex) {

                //If the length of the mesh is over the size of a unsigned short int (65535), then split the mesh
                float nofRequiredMeshes =   (float) mesh.vertexIndices.size() / 65535;
                uint32_t                    fromIndex = 0;
                uint32_t                    length = maxIndex;
                int                         nofChunks = 1;
                int                         currentMesh = 0;

                do {
                   
                    // create the list indices and vertex
                    std::vector<int> indexList(mesh.vertexIndices.begin() + fromIndex, mesh.vertexIndices.begin() + fromIndex + length);
                    std::vector<CPrefModelSink::vertex> verticesList(mesh.vertices.begin() + fromIndex, mesh.vertices.begin() + fromIndex + length);

                    //We need to adjust the values of the indices to the split of vertices selected
                    if (currentMesh > 0) for (int j = 0; j < indexList.size(); j++) indexList[j] -= (maxIndex * currentMesh);
                    //std::for_each(indicesList.begin(), indicesList.end(), [&](int& n) { n-(maxIndex * currentMesh); });
                    
                    // create the new mesh
                    CPrefModelSink::Mesh tMesh = {};
                    tMesh.meshName = mesh.meshName;
                    tMesh.meshId = mesh.meshId;
                    tMesh.vertexIndices = indexList;
                    tMesh.vertices = verticesList;

                    // add the mesh to the list of mesh
                    meshList.push_back(tMesh);

                    fromIndex += length;
                    length = std::min<uint32_t>(mesh.vertexIndices.size() - (nofChunks++ * maxIndex), maxIndex);

                } while (++currentMesh <= nofRequiredMeshes);
                
            } else { //The number of vertex indices is under 65535*/

                meshList.push_back(mesh);

            // }

            // accessorIndexS accIndex = accessorsMeshInstanceDict[meshInstance.meshIndex];
            
            // NOTE ABOUT SPLIT CODE : READ ABOVE. We should remove meshPieceIndex. 
            // int meshPieceIndex = 0; // This variables help us to know what "piece" of a splitted mesh we are actually

            // Add meshes and its materials
            for (CPrefModelSink::Mesh mesh : meshList) {

                // fx::gltf::Primitive gltfPrimitive;
                // gltfPrimitive.mode = fx::gltf::Primitive::Mode::Triangles;
                auto meshUSD = pxr::UsdGeomMesh::Define(usd_stage, pxr::SdfPath(mesh_USD.GetPath().GetString() + "/" + mesh.meshName));
                meshUSD.CreateSubdivisionSchemeAttr().Set(pxr::UsdGeomTokens->none); // line 636




                // If we inserted a mesh (position + indices + normals + uvs) in the buffer, we check the dictionary
                // To check if there is no mesh in the buffer, we see if the vector of indices index of the dictionary entry is empty
                // If there is no mesh in the buffer (indices index == -1), we call the function that adds the mesh in the buffer
                // We also call the function that adds the mesh in the buffer if the UVs are different for this mesh instance, but 
                // internally that function will check if the indices of the dict entry exist and it will not insert replicated data
                // If there is mesh in the buffer, just assign the indices of the gltf primitive to the same in the dictionary entry
                float uvFactor = getModelSink().getUVFactorFromMeshInstanceIndex(meshInstanceIndex);
                
                createMesh(meshUSD, mesh, 
                    // accIndex, 
                    uvFactor, 
                    // meshPieceIndex, 
                    meshInstance.meshIndex); 

                // [ACCESOR-BUFFER] 
                // colocamos este c�digo dentro de createMesh. Sobre todo porque me cargo por lo pronto lo del diccionario: accessorsMeshInstanceDict
                // gltfPrimitive.indices = accIndex.indicesAccIndex[meshPieceIndex];
                // gltfPrimitive.attributes["POSITION"] = accIndex.posAccIndex[meshPieceIndex];
                // if (accIndex.normalAccIndex.size() > 0)
                    // gltfPrimitive.attributes["NORMAL"] = accIndex.normalAccIndex[meshPieceIndex];
                // if (accIndex.UVFactorDictAccIndex[uvFactor].size() > 0)
                    // gltfPrimitive.attributes["TEXCOORD_0"] = accIndex.UVFactorDictAccIndex[uvFactor][meshPieceIndex];


               
                // Set vertex colors - 690. It is contemplate in the materials




                // Set material - 731

                // 3. make the material
                CPrefModelSink::Material material;
                getModelSink().getMaterialByIndex(meshInstance.materialIndex, material);

                //Check if there is a material Id 
                if (material.materialId != "") {

                    createMaterial(meshUSD, material);

                }

                // gltfMesh.primitives.push_back(gltfPrimitive);
                // meshPieceIndex++;
                


                // 4. add UsdPreviewSurface
                    
                // 5. add texturing



                // Bind skin data - 738
            }
            
            // TODO
            // document.meshes.push_back(gltfMesh);
            // document.nodes.push_back(gltfNode);
        }
    }

    // �? [NO_NECESSARY] This step is not necessary in USD
    // document.scenes.push_back(gltfScene);

    return true;
}

int PrefExportUSD::createMesh(pxr::UsdGeomMesh& meshUSD, const CPrefModelSink::Mesh& mesh, 
    // accessorIndexS& accIndex, 
    const float& uvFactor, 
    // const int& meshPieceNumber, 
    const int& origMeshIndex)
{
    // [ACCESOR-BUFFER] 
    // lo de crear el accessor y el buffer lo omito porque no lo considero necesario
    // da la sensaci�n que es m�s la manera como lo hace GLTF

    /* Indices */
    // accessorIndices.count = mesh.vertexIndices.size();

    // accessorIndices.max = { (float)mesh.vertexIndices.size() - 1 };

    // accessorIndices.type = fx::gltf::Accessor::Type::Scalar;
    // accessorIndices.componentType = fx::gltf::Accessor::ComponentType::UnsignedShort;



    /* Positions */
    // accessorP.count = mesh.vertices.size();

    // float minX, minY, minZ, maxX, maxY, maxZ; -  CHECK
    // prefModelSink.getMeshPositionMinMax(origMeshIndex, minX, minY, minZ, maxX, maxY, maxZ);
    // accessorP.min = { minX, minY, minZ };
    // accessorP.max = { maxX, maxY, maxZ };

    // accessorP.type = fx::gltf::Accessor::Type::Vec3;
    // accessorP.componentType = fx::gltf::Accessor::ComponentType::Float;

     // positions of each vertex in vertices - OK
     // encode(document.buffers.back().data, mesh.vertices[i].pos.x);
     // encode(document.buffers.back().data, mesh.vertices[i].pos.y);
     // encode(document.buffers.back().data, mesh.vertices[i].pos.z);



    //++++++++ [GLTF] - F +++++++++++
    // gltfPrimitive.indices = accIndex.indicesAccIndex[meshPieceIndex];
    // gltfPrimitive.attributes["POSITION"] = accIndex.posAccIndex[meshPieceIndex];
    //+++++++++++++++++++++++++++++++


    // get the positions from the mesh
    pxr::VtArray<pxr::GfVec3f> pos{};
    for (vertex v : mesh.vertices) {
        pos.push_back({ v.pos.x, v.pos.y, v.pos.z });
    }
    // emulate_double_sided : para esto primero se debe crear el material o saber al menos si tiene o no double size
    /*
        const Gltf::Material* const material = Gltf::GetById(cc_.gltf->materials, prim.material);
        const bool double_sided = material && material->doubleSided;
        const bool emulate_double_sided = double_sided && cc_.settings.emulate_double_sided;

        SkinData skin_data;
        const bool have_skin_data =
            skinned_mesh_context && prim_info.skin_index_stride != 0 &&
            GetSkinData(prim_info.skin_indices.data(), prim_info.skin_index_stride,
                prim_info.skin_weights.data(), prim_info.skin_weight_stride,
                cc_.gltf->nodes.size(), used_vert_count,
                skinned_mesh_context->gjoint_to_ujoint_map,
                skinned_mesh_context->gjoint_count, &skin_data);
    */
    bool emulate_double_sided = false;
    SetVertexValues(meshUSD.GetPointsAttr(), pos, emulate_double_sided);



    /* Normals */
    // if (prefModelSink.getMeshHasNormals(origMeshIndex))

    // accessorN.count = mesh.vertices.size();
    // accessorN.type = fx::gltf::Accessor::Type::Vec3;
    // accessorN.componentType = fx::gltf::Accessor::ComponentType::Float;

    // normal of each vertex in vertices
    // encode(document.buffers.back().data, mesh.vertices.normal.x);
    // encode(document.buffers.back().data, mesh.vertices.normal.y);
    // encode(document.buffers.back().data, mesh.vertices.normal.z);


    //++++++++ [GLTF] - F +++++++++++
    // if (accIndex.normalAccIndex.size() > 0)
    //   gltfPrimitive.attributes["NORMAL"] = accIndex.normalAccIndex[meshPieceIndex];
    //+++++++++++++++++++++++++++++++
    if (getModelSink().getMeshHasNormals(origMeshIndex)) {

        // get the normals from the mesh
        pxr::VtArray<pxr::GfVec3f> norms{};
        for (vertex v : mesh.vertices) {
            norms.push_back({ v.normal.x, v.normal.y, v.normal.z });
        }

        /*
          // this is not necessary because we do not handle with animation
          // If the normal has skin we apply it at the first frame of the animation
          VtArray<GfVec3f> skin_norms;
          if (have_skin_data && skinned_mesh_context &&
              skinned_mesh_context->bake_norm_mats) {
            // Bake normals to the first frame of the animation.
            skin_norms.resize(prim_info.norm.size());
            SkinNormals(skinned_mesh_context->bake_norm_mats, prim_info.norm.data(),
                        prim_info.norm.size(), skin_data.bindings.data(),
                        skin_norms.data());
            norms = &skin_norms;
          }
        */

        SetVertexValues(meshUSD.GetNormalsAttr(), norms, emulate_double_sided);
        meshUSD.SetNormalsInterpolation(pxr::UsdGeomTokens->vertex);
    }



    /* UV accessor */
    // accessorUV.count = mesh.vertices.size();
    // accessorUV.type = fx::gltf::Accessor::Type::Vec2;
    // accessorUV.componentType = fx::gltf::Accessor::ComponentType::Float;

    // Add UVs data to buffer
    // for (vertex v : mesh.vertices)
        // float UVu = v.uv.u;
        // float UVv = v.uv.v;
        // if (uvFactor > 0)
        // {
            // UVu = UVu / uvFactor;
            // UVv = UVv / uvFactor;
        // }
        // encode(document.buffers.back().data, UVu);
        // encode(document.buffers.back().data, UVv);
    

    //++++++++ [GLTF] - F +++++++++++
    // if (accIndex.UVFactorDictAccIndex[uvFactor].size() > 0)
    //   gltfPrimitive.attributes["TEXCOORD_0"] = accIndex.UVFactorDictAccIndex[uvFactor][meshPieceIndex];
    //+++++++++++++++++++++++++++++++

    // [QUESTION] In Fran's code, it explicitly add the condition (uvFactor > 0) in the next line (case 2) or 
    // during the division (case 1) does not make sense 
    // cases: 1. if we go inside it means uvFactor is higher than 0 and the line during the division is not necessary
    //        2. whenever the uvFactor is equal to zero we do not store the uv of the vertices
    // It makes more sense for me the case 2. 
    if (getModelSink().getMeshHasUVs(origMeshIndex)) {

        float UVu, UVv;

        // get the uv from the mesh
        pxr::VtArray<pxr::GfVec2f> uv{};
        for (vertex v : mesh.vertices) {

            UVu = v.uv.u;
            UVv = v.uv.v;
            if (uvFactor > 0) {
                UVu = UVu / uvFactor;
                UVv = UVv / uvFactor;
            }

            uv.push_back({ UVu, UVv });
        }

        // [QUESTION] los UVs se ven afectados por los materiales?. M�s bien, se deber�a generar un uv por material aplicando la transformaci�n al uv ?
            /*
            const PrimInfo::Uvset& src_uv = uv_found->second;
            const PrimInfo::Uvset* uv = &src_uv;
            PrimInfo::Uvset transformed_uv;
            const Gltf::Material::Texture::Transform& transform =
                uvset_kv.second.transform;
            if (!transform.IsIdentity()) {
                transformed_uv = src_uv;
                TransformUvs(transform, transformed_uv.size(), transformed_uv.data());
                uv = &transformed_uv;
            }
            const pxr::TfToken uvset_tok(AppendNumber("st", number));
            const pxr::UsdGeomPrimvar uvs_primvar = meshUSD.CreatePrimvar(uvset_tok, pxr::SdfValueTypeNames->TexCoord2fArray, pxr::UsdGeomTokens->vertex);
            */

        const pxr::TfToken uvset_tok(AppendNumber("st", 0));
        const pxr::UsdGeomPrimvar uvs_primvar = meshUSD.CreatePrimvar(uvset_tok, pxr::SdfValueTypeNames->TexCoord2fArray, pxr::UsdGeomTokens->vertex);

        SetVertexValues(uvs_primvar, uv, emulate_double_sided);
    }


    // Set point extent from its AABB - 724. bound points

    // we can have two implementations, which in should return the same values
    // one that considers the min and max from the preference's scene
    float minX, minY, minZ, maxX, maxY, maxZ; 
    getModelSink().getMeshPositionMinMax(origMeshIndex, minX, minY, minZ, maxX, maxY, maxZ);
    pxr::GfVec3f min = { minX, minY, minZ };
    pxr::GfVec3f max = { maxX, maxY, maxZ };
    const pxr::VtArray<pxr::GfVec3f> extent({min, max});

    // a second one that calculate the min and max from the position.
    // const pxr::GfRange3f aabb = BoundPoints(pos.data(), pos.size());
    // const pxr::VtArray<pxr::GfVec3f> extent({aabb.GetMin(), aabb.GetMax()});

    meshUSD.GetExtentAttr().Set(extent);


    return 1;
}

pxr::GfRange3f PrefExportUSD::BoundPoints(const pxr::GfVec3f* points, size_t point_count) {

    pxr::GfRange3f aabb;

    for (size_t point_index = 0; point_index != point_count; ++point_index) {
        aabb.UnionWith(points[point_index]);
    }

    return aabb;
}

void PrefExportUSD::createMaterial(pxr::UsdGeomMesh meshUSD, CPrefModelSink::Material material) {
    
    int gltfMaterialIndex;

    pxr::UsdShadeMaterial materialUSD;

    const auto insert_result = materials_.insert(std::make_pair(material.materialName, materialUSD));

    materialUSD = insert_result.first->second;

    // if the material is not inserted or created
    if (insert_result.second == true) {
        
        // create the material 
        materialUSD = pxr::UsdShadeMaterial::Define(usd_stage,
            pxr::SdfPath(meshUSD.GetPath().GetString() + "/material" + material.materialName));

        
        // 4. Add a UsdPreviewSurface
        pxr::UsdShadeShader shader = fillInMaterial(materialUSD, material);


       // get the path of the material
       auto material_path = materialUSD.GetPath().GetString();


       // 5. Add texturing
       // -�Funcional code. Remains to integrate to a preference's scene

       // auto reader = attach_texture(usd_stage, shader, material_path);

       // auto st_input = materialUSD.CreateInput(pxr::TfToken("frame:stPrimvarName"), pxr::SdfValueTypeNames->Token);

       // st_input.Set(pxr::TfToken("st"));

       // reader.CreateInput(pxr::TfToken("varname"), pxr::SdfValueTypeNames->Token).ConnectToSource(st_input);


    }

}

pxr::UsdShadeShader PrefExportUSD::fillInMaterial(pxr::UsdShadeMaterial &materialUSD, CPrefModelSink::Material material){

    // get the path of the material
    auto material_path = materialUSD.GetPath().GetString();

    // generate the UsdShadeShader 
    auto shader = pxr::UsdShadeShader::Define(usd_stage, pxr::SdfPath(material_path+"/"+"PBRShader"));
    shader.CreateIdAttr(pxr::VtValue(pxr::TfToken("UsdPreviewSurface")));

    const char* texFind = "metallic";
    if (material.numParameterDict.find(texFind) != material.numParameterDict.end())
        shader.CreateInput(pxr::TfToken(texFind), pxr::SdfValueTypeNames->Float).Set(material.numParameterDict[texFind]);

    texFind = "roughness";
    if (material.numParameterDict.find(texFind) != material.numParameterDict.end())
        shader.CreateInput(pxr::TfToken(texFind), pxr::SdfValueTypeNames->Float).Set(material.numParameterDict[texFind]);

    /*if (material.numParameterDict.find("base_color0") != material.numParameterDict.end())
        gltfMaterial.pbrMetallicRoughness.baseColorFactor[0] = material.numParameterDict["base_color0"];
    if (material.numParameterDict.find("base_color1") != material.numParameterDict.end())
        gltfMaterial.pbrMetallicRoughness.baseColorFactor[1] = material.numParameter
        Dict["base_color1"];
    if (material.numParameterDict.find("base_color2") != material.numParameterDict.end())
        gltfMaterial.pbrMetallicRoughness.baseColorFactor[2] = material.numParameterDict["base_color2"];
    if (material.numParameterDict.find("base_color3") != material.numParameterDict.end())
        gltfMaterial.pbrMetallicRoughness.baseColorFactor[3] = material.numParameterDict["base_color3"];

    // se crean las texturas
    if (material.uriDict.find("base_color") != material.uriDict.end())
    {
        fx::gltf::Material::Texture gltfMaterialTextureBaseColor;
        fx::gltf::Texture gltfTextureBaseColor;
        fx::gltf::Image gltfImageBaseColor;

        gltfImageBaseColor.uri = material.uriDict["base_color"];

        gltfMaterialTextureBaseColor.index = document.images.size();
        document.images.push_back(gltfImageBaseColor);

        gltfTextureBaseColor.source = document.textures.size();
        document.textures.push_back(gltfTextureBaseColor);

        gltfMaterial.pbrMetallicRoughness.baseColorTexture = gltfMaterialTextureBaseColor;
    }

    if (material.uriDict.find("metallic") != material.uriDict.end())
    {

        fx::gltf::Material::Texture gltfMaterialTextureMetallic;
        fx::gltf::Texture gltfTextureMetallic;
        fx::gltf::Image gltfImageMetallic;

        gltfImageMetallic.uri = material.uriDict["metallic"];

        gltfMaterialTextureMetallic.index = document.images.size();
        document.images.push_back(gltfImageMetallic);

        gltfTextureMetallic.source = document.textures.size();
        document.textures.push_back(gltfTextureMetallic);

        gltfMaterial.pbrMetallicRoughness.metallicRoughnessTexture = gltfMaterialTextureMetallic;
    }

    if (material.uriDict.find("occlusion") != material.uriDict.end())
    {
        fx::gltf::Material::OcclusionTexture gltfMaterialTextureOcclusion;
        fx::gltf::Texture gltfTextureOcclusion;
        fx::gltf::Image gltfImageOcclusion;

        gltfImageOcclusion.uri = material.uriDict["occlusion"];

        gltfMaterialTextureOcclusion.index = document.images.size();
        document.images.push_back(gltfImageOcclusion);

        gltfTextureOcclusion.source = document.textures.size();
        document.textures.push_back(gltfTextureOcclusion);

        gltfMaterial.occlusionTexture = gltfMaterialTextureOcclusion;
    }

    if (material.uriDict.find("normal") != material.uriDict.end())
    {
        fx::gltf::Material::NormalTexture gltfMaterialTextureNormal;
        fx::gltf::Texture gltfTextureNormal;
        fx::gltf::Image gltfImageNormal;

        gltfImageNormal.uri = material.uriDict["normal"];

        gltfMaterialTextureNormal.index = document.images.size();
        document.images.push_back(gltfImageNormal);

        gltfTextureNormal.source = document.textures.size();
        document.textures.push_back(gltfTextureNormal);

        gltfMaterial.normalTexture = gltfMaterialTextureNormal;
    }

    if (material.uriDict.find("emissive") != material.uriDict.end())
    {
        fx::gltf::Material::Texture gltfMaterialTextureEmissive;
        fx::gltf::Texture gltfTextureEmissive;
        fx::gltf::Image gltfImageEmissive;

        gltfImageEmissive.uri = material.uriDict["emissive"];

        gltfMaterialTextureEmissive.index = document.images.size();
        document.images.push_back(gltfImageEmissive);

        gltfTextureEmissive.source = document.textures.size();
        document.textures.push_back(gltfTextureEmissive);

        gltfMaterial.emissiveTexture = gltfMaterialTextureEmissive;
    }

    if (material.numParameterDict.find("emissive0") != material.numParameterDict.end())
        gltfMaterial.emissiveFactor[0] = material.numParameterDict["emissive0"];
    if (material.numParameterDict.find("emissive1") != material.numParameterDict.end())
        gltfMaterial.emissiveFactor[1] = material.numParameterDict["emissive1"];
    if (material.numParameterDict.find("emissive2") != material.numParameterDict.end())
        gltfMaterial.emissiveFactor[2] = material.numParameterDict["emissive2"];

    if (material.strParameterDict.find("alphaMode") != material.strParameterDict.end())
    {
        if (material.strParameterDict["alphaMode"] == "OPAQUE")
            gltfMaterial.alphaMode = fx::gltf::Material::AlphaMode::Opaque;
        if (material.strParameterDict["alphaMode"] == "BLEND")
            gltfMaterial.alphaMode = fx::gltf::Material::AlphaMode::Blend;
        if (material.strParameterDict["alphaMode"] == "MASK")
            gltfMaterial.alphaMode = fx::gltf::Material::AlphaMode::Mask;
    }

    if (material.numParameterDict.find("alphaCutoff") != material.numParameterDict.end())
        gltfMaterial.alphaCutoff = material.numParameterDict["alphaCutoff"];

    if (material.strParameterDict.find("doubleSided") != material.strParameterDict.end())
    {
        std::string doubleSided = material.strParameterDict["doubleSided"];
        std::for_each(doubleSided.begin(), doubleSided.end(), [](char& c) {c = ::toupper(c); });
        if (doubleSided == "TRUE") gltfMaterial.doubleSided = true;
        else gltfMaterial.doubleSided = false;
    }

    if (material.materialName != "")
        gltfMaterial.name = material.materialName;
    */

    materialUSD.CreateSurfaceOutput().ConnectToSource(shader, pxr::TfToken("surface"));

    return shader;
}


pxr::UsdShadeShader PrefExportUSD::attach_texture(pxr::UsdStageRefPtr& stage,
    pxr::UsdShadeShader shader,
    std::string const& material_path,
    std::string const reader_name,
    std::string const shader_name) {

    auto reader = pxr::UsdShadeShader::Define(stage, pxr::SdfPath(material_path + "/" + reader_name));
    reader.CreateIdAttr(pxr::VtValue(pxr::TfToken("UsdPrimvarReader_float2")));

    char file[256];
    sprintf(file, "%s%s%s", ASSET_DIRECTORY, OS_SEP, "USDLogoLrg.png");

    auto sampler = pxr::UsdShadeShader::Define(
        stage, pxr::SdfPath(material_path + "/" + shader_name));
    sampler.CreateIdAttr(pxr::VtValue(pxr::TfToken("UsdUVTexture")));
    sampler.CreateInput(pxr::TfToken("file"), pxr::SdfValueTypeNames->Asset)
        .Set(pxr::SdfAssetPath(file));
    sampler.CreateInput(pxr::TfToken("st"), pxr::SdfValueTypeNames->Float2)
        .ConnectToSource(reader, pxr::TfToken("result"));
    sampler.CreateOutput(pxr::TfToken("rgb"), pxr::SdfValueTypeNames->Float3);
    shader.CreateInput(pxr::TfToken("diffuseColor"),
        pxr::SdfValueTypeNames->Color3f)
        .ConnectToSource(sampler, pxr::TfToken("rgb"));

    return reader;
}









// ##### Example codes #####

void PrefExportUSD::example1() {

    auto stage = pxr::UsdStage::CreateInMemory();
    stage->GetRootLayer()->SetDocumentation("This is an example of adding a comment. You can add comments inside any pair of ()s");

    auto sphere = pxr::UsdGeomSphere::Define(stage, pxr::SdfPath("/SomeSphere"));
    sphere.GetPrim().SetMetadata(
        pxr::SdfFieldKeys->Comment,
        "I am a comment"
    );

    auto* result = new std::string();
    stage->GetRootLayer()->ExportToString(result);
    std::cout << *result << std::endl;
    delete result;
    result = nullptr;

}

void PrefExportUSD::example2() {
    auto stage = pxr::UsdStage::CreateInMemory();
    auto some_sphere = pxr::UsdGeomSphere::Define(stage, pxr::SdfPath("/SomeSphere"));

    // Method A: Set using methods
    auto model = pxr::UsdModelAPI(some_sphere.GetPrim());
    model.SetAssetName("some_asset");
    model.SetAssetVersion("v1");
    model.SetAssetIdentifier(pxr::SdfAssetPath("some/path/to/file.usda"));
    model.SetPayloadAssetDependencies(pxr::VtArray<pxr::SdfAssetPath> {
        pxr::SdfAssetPath("something.usd"),
            pxr::SdfAssetPath("another/thing.usd"),
    });

    // Method B: Set-by-key
    auto another_sphere = pxr::UsdGeomSphere::Define(stage, pxr::SdfPath("/AnotherSphere"));
    auto another_prim = another_sphere.GetPrim();
    another_prim.SetAssetInfoByKey(
        pxr::UsdModelAPIAssetInfoKeys->version,
        pxr::VtValue("v1")
    );
    another_prim.SetAssetInfoByKey(
        pxr::UsdModelAPIAssetInfoKeys->name,
        pxr::VtValue("some_asset")
    );
    another_prim.SetAssetInfoByKey(
        pxr::UsdModelAPIAssetInfoKeys->identifier,
        pxr::VtValue("some/path/to/file.usda")
    );
    another_prim.SetAssetInfoByKey(
        pxr::UsdModelAPIAssetInfoKeys->payloadAssetDependencies,
        pxr::VtValue{ pxr::VtArray<pxr::SdfAssetPath> {
            pxr::SdfAssetPath("something.usd"),
            pxr::SdfAssetPath("another/thing.usd"),
        } }
    );

    // Method C: Set-by-dict
    auto last_sphere = pxr::UsdGeomSphere::Define(stage, pxr::SdfPath("/LastSphere"));
    last_sphere.GetPrim().SetAssetInfo(pxr::VtDictionary{
            {pxr::TfToken("identifier"), pxr::VtValue("some/path/to/file.usda")},
            {pxr::TfToken("name"), pxr::VtValue("some_asset")},
            {pxr::TfToken("version"), pxr::VtValue("v1")},
            {pxr::TfToken("payloadAssetDependencies"), pxr::VtValue{pxr::VtArray<pxr::SdfAssetPath> {
                pxr::SdfAssetPath("something.usd"),
                pxr::SdfAssetPath("another/thing.usd"),
            }}}
        }
    );

    auto* result = new std::string();
    stage->GetRootLayer()->ExportToString(result);
    std::cout << *result << std::endl;
    delete result;
    result = nullptr;
}


pxr::UsdGeomMesh PrefExportUSD::attach_billboard(pxr::UsdStageRefPtr& stage,
    std::string const& root,
    std::string const& name) {

    auto billboard = pxr::UsdGeomMesh::Define(stage, pxr::SdfPath(root + "/" + name));

    /* Three cases to do the same */
    // case 1
    billboard.CreatePointsAttr(pxr::VtValue{ pxr::VtArray<pxr::GfVec3f> {
        {-430, -145, 0},
        {430, -145, 0},
        {430, 145, 0},
        {-430, 145, 0},
    } });

    pxr::VtArray<pxr::GfVec3f> pos{};
    pos.push_back({ -430, -145, 0 });
    pos.push_back({ 430, -145, 0 });
    pos.push_back({ 430, 145, 0 });
    pos.push_back({ -430, 145, 0 });

    // case 2
    pxr::VtValue pos_value{pos};
    billboard.CreatePointsAttr(pos_value);

    // case 3 - template function : we are using this one because we want to emulate the double sided
    SetVertexValues(billboard.GetPointsAttr(), pos, false);


    // test the normals
    SetVertexValues(billboard.GetNormalsAttr(), pos, false);
    billboard.SetNormalsInterpolation(pxr::UsdGeomTokens->vertex);


    // test the UVs
    float UVu, UVv;

    // get the uv from the mesh
    pxr::VtArray<pxr::GfVec2f> uv{};

    uv.push_back({ -430, 0 });
    uv.push_back({ 430, 0 });
    uv.push_back({ 430, 0 });
    uv.push_back({ -430, 0 });

    const pxr::TfToken uvset_tok(AppendNumber("st", 0));
    const pxr::UsdGeomPrimvar uvs_primvar = billboard.CreatePrimvar(uvset_tok, pxr::SdfValueTypeNames->TexCoord2fArray, pxr::UsdGeomTokens->vertex);

    SetVertexValues(uvs_primvar, uv, false);



    billboard.CreateFaceVertexCountsAttr(pxr::VtValue{ pxr::VtArray<int> {4} });
    billboard.CreateFaceVertexIndicesAttr(pxr::VtValue{ pxr::VtArray<int> {0, 1, 2, 3} });
    billboard.CreateExtentAttr(pxr::VtValue{ pxr::VtArray<pxr::GfVec3f> {
        {
            {-430, -145, 0},
            {430, 145, 0},
        }
    } });

    auto coordinates = billboard.CreatePrimvar(
        pxr::TfToken("st"), // name 
        pxr::SdfValueTypeNames->TexCoord2fArray, // type of coordinate
        pxr::UsdGeomTokens->varying); // interpolation

    coordinates.Set(pxr::VtValue{ pxr::VtArray<pxr::GfVec2f> {
        {0, 0},
        {1, 0},
        {1, 1},
        {0, 1},
    } });

    return billboard;
}

pxr::UsdShadeShader attach_surface_shader(pxr::UsdStageRefPtr& stage,
    pxr::UsdShadeMaterial material,
    pxr::SdfPath const& path) {

    auto shader = pxr::UsdShadeShader::Define(stage, path);
    shader.CreateIdAttr(pxr::VtValue(pxr::TfToken("UsdPreviewSurface")));
    //shader.CreateInput(pxr::TfToken("roughness"), pxr::SdfValueTypeNames->Float).Set(0.4f);
    //shader.CreateInput(pxr::TfToken("metallic"), pxr::SdfValueTypeNames->Float).Set(0.0f);

    // shader

    material.CreateSurfaceOutput().ConnectToSource(shader, pxr::TfToken("surface"));

    return shader;
}

pxr::UsdShadeShader attach_texture(pxr::UsdStageRefPtr& stage,
    pxr::UsdShadeShader shader,
    std::string const& material_path,
    std::string const reader_name = "stReader",
    std::string const shader_name = "diffuseTexture") {
    auto reader = pxr::UsdShadeShader::Define(stage, 
        pxr::SdfPath(material_path + "/" + reader_name));
    reader.CreateIdAttr(pxr::VtValue(pxr::TfToken("UsdPrimvarReader_float2")));

    char file[256];
    sprintf(file, "%s%s%s", ASSET_DIRECTORY, OS_SEP, "USDLogoLrg.png");

    auto sampler = pxr::UsdShadeShader::Define(
        stage, pxr::SdfPath(material_path + "/" + shader_name));
    sampler.CreateIdAttr(pxr::VtValue(pxr::TfToken("UsdUVTexture")));
    sampler.CreateInput(pxr::TfToken("file"), pxr::SdfValueTypeNames->Asset)
        .Set(pxr::SdfAssetPath(file));
    sampler.CreateInput(pxr::TfToken("st"), pxr::SdfValueTypeNames->Float2)
        .ConnectToSource(reader, pxr::TfToken("result"));
    sampler.CreateOutput(pxr::TfToken("rgb"), pxr::SdfValueTypeNames->Float3);
    shader.CreateInput(pxr::TfToken("diffuseColor"),
        pxr::SdfValueTypeNames->Color3f)
        .ConnectToSource(sampler, pxr::TfToken("rgb"));

    return reader;
}

void PrefExportUSD::example3MeshWithMaterials () {

    // 1. Making a model
    auto stage = pxr::UsdStage::CreateNew("simpleShading_6.usd");
    pxr::UsdGeomSetStageUpAxis(stage, pxr::TfToken("Y"));

    auto root = pxr::UsdGeomXform::Define(stage, pxr::SdfPath("/TexModel"));
    pxr::UsdModelAPI(root).SetKind(pxr::KindTokens->component);


    // 2. Adding a Mesh
    auto billboard = attach_billboard(stage, root.GetPath().GetString());
    
    // assign color to mesh
    pxr::VtArray<pxr::GfVec3f> colors;
    colors.push_back(pxr::GfVec3f(0.0f, 1.0f, 0.0f));
    billboard.CreateDisplayColorAttr(pxr::VtValue(colors));

    
    // 3. Make a material
    auto material = pxr::UsdShadeMaterial::Define(stage, pxr::SdfPath(billboard.GetPath().GetString() + "/" + "boardMat"));
    

    // 4. Add a UsdPreviewSurface
    auto material_path = material.GetPath().GetString();
    auto shader = attach_surface_shader(stage, material, pxr::SdfPath(material_path + "/" + "PBRShader"));



    // 5. Add texturing
        // add texture
        auto reader = attach_texture(stage, shader, material_path);


        auto st_input = material.CreateInput(pxr::TfToken("frame:stPrimvarName"), pxr::SdfValueTypeNames->Token);

        st_input.Set(pxr::TfToken("st"));

        reader.CreateInput(pxr::TfToken("varname"), pxr::SdfValueTypeNames->Token).ConnectToSource(st_input);
        

    // bind the mesh to our material
    pxr::UsdShadeMaterialBindingAPI(billboard).Bind(material);

    
    // save the results
    auto* result = new std::string();
    stage->GetRootLayer()->ExportToString(result);
    std::cout << *result << std::endl;

    stage->GetRootLayer()->Save();
    delete result;
    result = nullptr;
}

