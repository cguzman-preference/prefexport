#include "PrefExportFactory.h"

PrefExportBase* PrefExportFactory::Create(ExportFileFormats format){

	switch (format) {
		case USD: return new PrefExportUSD();
	}

}


int main(int argc, char* argv[]) {

    PrefExportFactory creator;

    PrefExportBase* el = creator.Create(ExportFileFormats::USD);

    // el->exportDocument();

    return 0;
}
