/**
 * @file PrefExportBase.cpp
 *
 * @brief This file contains the common code
 *
 * @ingroup PrefExportBase
 * (Note: this needs exactly one @defgroup somewhere)
 *
 * @author Cesar Augusto Guzman ALvarez
 * Contact: cguzman@preference.es
 *
 */
#include "PrefExportBase.h"

// for information see its associated .h

PrefExportBase::PrefExportBase() : file_path_(PREF_EXPORT_PATH_FILE) {
	
}

PrefExportBase::PrefExportBase(CPrefModelSink modelSink, const char* file_path) : modelSink_(modelSink), file_path_(file_path) {
	 
}



bool PrefExportBase::exportDocument() {
	printf("Inside the class PrefExportBase");
	return false; // TODO
}


bool PrefExportBase::addScenes(int sceneIndex) {

    std::vector<std::string> materialIndexVector;
    int index = 0;

    // If sceneIndex is -1, we write all scenes of the sink in the file
    if (sceneIndex == -1) {

        // iterate for all the scenes
        std::vector<CPrefModelSink::Scene> sceneList = getModelSink().getScenes();
        for (CPrefModelSink::Scene scene : sceneList) {
            addScene(scene);
        }

    } else { // Otherwise, we get the given scene and store it in the file

        // validate that the index is in the range of the vector
        if (sceneIndex < 0 || sceneIndex >= getModelSink().getScenes().size())
            return false;

        // get the scene 
        CPrefModelSink::Scene scene = getModelSink().getScenes()[sceneIndex];
        return addScene(scene);

    }

    return true;
}

bool PrefExportBase::addScene(CPrefModelSink::Scene scene) { return false; };


std::string PrefExportBase::AppendNumber(const char* prefix, size_t prefix_len, size_t index) {

    const size_t len_max = prefix_len + 24;
    std:string text(len_max, 0);
    const size_t len = snprintf(&text[0], len_max, "%s%zu", prefix, index);
    text.resize(len);
    return text;

}

std::string PrefExportBase::AppendNumber(const char* prefix, size_t index) {
    return AppendNumber(prefix, strlen(prefix), index);
}

std::string PrefExportBase::AppendNumber(const std::string& prefix, size_t index) {
    return AppendNumber(prefix.c_str(), prefix.length(), index);
}

const char* PrefExportBase::getPExportVersion() {
	return preferenceExportVersionString;
}

const char* PrefExportBase::getFilePath() {
	return file_path_;
}

CPrefModelSink PrefExportBase::getModelSink() {
	return modelSink_;
}

void PrefExportBase::setModelSink(CPrefModelSink modelSink) {
	modelSink_ = modelSink;
}
