/**
 * @file PrefExportBase.h
 *
 * @brief This file contains the common code
 *
 * @ingroup PrefExportBase
 * (Note: this needs exactly one @defgroup somewhere)
 *
 * @author Cesar Augusto Guzman ALvarez
 * Contact: cguzman@preference.es
 *
 */
#ifndef PREF_EXPORT_EXPORT_BASE_
#define PREF_EXPORT_EXPORT_BASE_


// #ifndef PREF_EXPORT_PATH_FILE
#define PREF_EXPORT_PATH_FILE "TEST_FILE.usda"
// #endif // !PREF_EXPORT_PATH_FILE

// #ifndef PREF_EXPORT_EXPORT_NORMALS
#define PREF_EXPORT_EXPORT_NORMALS true
// #endif // !PREF_EXPORT_EXPORT_NORMALS

// #ifndef PREF_EXPORT_EXPORT_MATERIALS
#define PREF_EXPORT_EXPORT_MATERIALS true
// #endif // !PREF_EXPORT_EXPORT_MATERIALS

// Includes
#include <iostream>
#include <string>

#include <map>
#include <unordered_map>

#include <stdexcept>

#include <assert.h>     /* assert */
// TO SOLVE THE ERRROR ASSERT : IDENTIFIER NOT FOUND
#define ASSERT ATLASSERT
#include "atltypes.h"

#include "PrefModelSink.h"

typedef Pref::CModelSink<float, boost::numeric::ublas::matrix<float>>::vertex vertex;

#include <ExportFileFormatDefinitions.h>


// using namespace Pref;



/**
* This class implements the common behaviors to export any scene to a different format
* It is more like an Abstract class
*
* author: Cesar Guzman Alvarez
* mail: cguzman@preference.es
* created date: 17/12/2020
* last modified: 30/12/2020
* version: 0.0.2
**/
class PrefExportBase
{

public:
	// ------------------
	// Public section
	// ------------------

	// ###### CONSTRUCTOR & DESTRUCTOR #######

		// Initialises the parameters file_path and params
		PrefExportBase();

		/**
		* @param CPrefModelSink input file with the preference Scene
		* @param file_path : file to store the usd
		* @param ExportParams : variable to use for configuration parameters. e.g., to activate the creation of materials
		*/
		PrefExportBase(CPrefModelSink modelSink, const char* file_path);

		// destructor
		virtual ~PrefExportBase() = default;



	// ###### MAIN FUNCTION TO EXECUTE THE EXPORT #######

		virtual bool exportDocument();
		virtual bool addScenes(int sceneIndex = -1);
		virtual bool addScene(CPrefModelSink::Scene scene);


		std::string AppendNumber(const char* prefix, size_t prefix_len, size_t index); 
		std::string AppendNumber(const char* prefix, size_t index);
		std::string AppendNumber(const std::string& prefix, size_t index);


	// ###### SET AND GET METHODS #######

		/** Retrieve the version of the export usd
		*/
		const char* getPExportVersion();

		/** Retrieve the name and path of the file
		*/
		const char* getFilePath();

		/** Retrieve the preference's scene
		*/
		CPrefModelSink getModelSink();

		/** Set the preference's scene 
		* */
		void setModelSink(CPrefModelSink modelSink);

private:
	// ------------------
	// Private Section 
	// ------------------

	// ###### PARAMETERS #######
	
		/**
		* Input parameter that represents the Preference Scene
		*/
		CPrefModelSink modelSink_;

		/**
		* Full complete path and name of the usd file to generate
		*/
		const char* file_path_ = NULL;

		/**
		* String with the current version of the export program
		*/
		const char* preferenceExportVersionString = "0.0.2";

protected:
	// ------------------
	// Protected section
	// ------------------


};

#endif // !PREF_EXPORT_EXPORT_BASE_