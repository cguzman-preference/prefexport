# README #

Translator between preference 3D scene into format USD in C++:

## What is this repository for? ##

* This repository contains the necessary information to develop a general translator between preference 3D scene into any format USD in C++. 

* We solve the following first steps:

	* Find examples by Internet. Sandbox and USD viewer
	* Check wheter USD is defined as a level of maya or vertixes
	* Focus in Triangle, instances of triangles, materials and colors. 

* Current Version 0.0.0
* [Learn USD]( https://graphics.pixar.com/usd/docs/index.html )

## How do I get set up? ##


* It was tested in Visual Studio 2019.


## History of development ##

The history of the development can be found in the file HISTORY
